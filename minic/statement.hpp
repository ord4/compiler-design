#pragma once

#include "expression.hpp"
#include "declaration.hpp"

class Statement {
  Type* type;
};

class Break_Stmt : public Statement {};

class Continue_Stmt : public Statement {};

class Compound_Stmt : public Statement {
public:
  // TODO: How to construct this list of statements
  Compound_Stmt(std::vector<Type*>& v) {
    //stmts = v
  }

  // Return a statement from the block given a valid index
  Statement* get_statement(int i) const {
    return stmts[i];
  }

private:
  std::vector<Statement*> stmts;
};

class While_Stmt : public Statement {
public:
  While_Stmt(Expression* e, Statement* s) {
    conditional = e;
    body = s;
  }

  Expression* get_conditional() const {
    return conditional;
  }

  Statement* get_body() const {
    return body;
  }

private:
  Expression* conditional;
  Statement* body;
};

// If statement given in the form of:
// if e then s1, else s2
// Where s1 is the statement read if conditional evaluates to true
class If_Stmt : public Statement {
public:
  If_Stmt(Expression* e, Statement* s1, Statement* s2) {
    conditional = e;
    if_true = s1;
    if_false = s2;
  }

  Expression* get_conditional() const {
    return conditional;
  }

  Statement* get_true_statment() const {
    return if_true;
  }

  Statement* get_false_statment() const {
    return if_false;
  }

private:
  Expression* conditional;
  Statement* if_true;
  Statement* if_false;
};

class Return_Stmt : public Statement {
public:
  Return_Stmt(Expression* e) {
    ret = e;
  }

  Expression* get_return_expr() const {
    return ret;
  }

private:
  Expression* ret;
};

class Expression_Stmt : public Statement {
public:
  Expression_Stmt(Expression* e) {
    expr = e;
  }

  Expression* get_expression() const {
    return expr;
  }

private:
  Expression* expr;
};

class Local_Def_Stmt : public Statement {
public:
  Local_Def_Stmt(Declaration* d) {
    decl = d;
  }

  Declaration* get_declaration() const {
    return decl;
  }

private:
  Declaration* decl;
};
