#pragma once

#include <unordered_set>

class Symbol {
public:
  Symbol() {}
  
  Symbol(std::string const* s) {
    str = s;
  }

  std::string const& get_symbol() const { return *str; }
  
private:
  std::string const* str;
};

class SymbolTable : std::unordered_set<std::string> {
public:
  // Return the symbol that matches the key
  Symbol get(std::string const& str) { return &*emplace(str).first;  }
  Symbol get(char* const str) {  return &*emplace(str).first;  }
};


