#pragma once

#include "symbol.hpp"

class Token {
public:
  enum Name {
	     lbrace,
	     rbrace,
	     lparen,
	     rparen,
	     colon,
	     semicolon,
	     comma,
	     arrow,
	     plus,
	     bang,
	     minus,
	     star,
	     slash,
	     percent,
	     question,
	     equal,
	     equal_equal,
	     bang_equal,
	     less,
	     greater,
	     less_equal,
	     greater_equal,
	     and_kw,
	     bool_kw,
	     break_kw,
	     continue_kw,
	     else_kw,
	     false_kw,
	     fun_kw,
	     if_kw,
	     int_kw,
	     not_kw,
	     or_kw,
	     ref_kw,
	     return_kw,
	     true_kw,
	     var_kw,
	     while_kw,
	     integer_literal,
	     float_literal,
	     boolean_literal,
	     identifier,
	     end_of_file,
  };

  Token() {
    Token(end_of_file, Symbol());
  }

  Token(Name n, Symbol s) {
    name = n;
    sym = s;
  }

  Name get_name() const { return name; }

  Symbol get_symbol() const { return sym; }

  bool is_eof() const { return name == Name::end_of_file; }

  //  Location get_location() const { return loc; }

private:
  Name name;
  Symbol sym;
  // FIXME: What to do about a location within the soure?
  //  Location loc;
};
