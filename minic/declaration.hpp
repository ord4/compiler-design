#pragma once

#include <list>

class Declaration {};

class Object_Def : public Declaration {
public:
  Object_Def(std::string& n, Type* t, Expression* e) {
    name = n;
    type = t;
    expr = e;
  }

  std::string get_obj_name() const {
    return name;
  }

  Type* get_obj_type() const {
    return type;
  }

  Expression* get_obj_expr() const {
    return expr;
  }

private:
  std::string name;
  Type* type;
  Expression* expr;
};

class Reference_Def : public Declaration {
public:
  Reference_Def(std::string& n, Type* t, Expression* e) {
    name = n;
    type = t;
    expr = e;
  }

  std::string get_ref_name() const {
    return name;
  }

  Type* get_ref_type() const {
    return type;
  }

  Expression* get_ref_expr() const {
    return expr;
  }

private:
  std::string name;
  Type* type;
  Expression* expr;
};

// TODO: How to put all the elements into a list?
class Function_Def : public Declaration {
public:
  Function_Def(std::string& n, Type* t) {
    name = n;
    ret_type = t;
  }

private:
  std::string name;
  Type* ret_type;
  // Need some list of declarations to make up the constructor
  // std::list<Declaration*> params;
};
