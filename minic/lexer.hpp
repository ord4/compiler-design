#pragma once

#include <unordered_map>

#include "token.hpp"

class Lexer {
public:
  Lexer(SymbolTable& symt, char const* f, char const* l) {
    sym_table = &symt;
    first = f;
    limit = l;
    src_line = 1;

    // Populate our keywords
    kws.emplace("and", Token::and_kw);
    kws.emplace("bool", Token::bool_kw);
    kws.emplace("break", Token::break_kw);
    kws.emplace("continue", Token::continue_kw);
    kws.emplace("else", Token::else_kw);
    kws.emplace("false", Token::false_kw);
    kws.emplace("fun", Token::fun_kw);
    kws.emplace("if", Token::if_kw);
    kws.emplace("int", Token::int_kw);
    kws.emplace("not", Token::not_kw);
    kws.emplace("or", Token::or_kw);
    kws.emplace("ref", Token::ref_kw);
    kws.emplace("return", Token::return_kw);
    kws.emplace("true", Token::true_kw);
    kws.emplace("while", Token::while_kw);
    kws.emplace("var", Token::var_kw);  
  }
  
  Lexer(SymbolTable& symt, std::string const& str) {
    Lexer(symt, str.data(), str.data() + str.size());
  }
  
  Token get_next_token() {
    while(!is_end_of_file()) {
      switch(peek()) {
	// Match tokens
      case ' ': // Do nothing, Why?

      case '\t':
	consume();
	continue;

      case '\n':
	src_line++;
	consume();
	continue;

      case '{':
	return match(Token::lbrace, 1);

      case '}':
	return match(Token::rbrace, 1);

      case '(':
	return match(Token::lparen, 1);

      case ')':
	return match(Token::rparen, 1);

      case ':':
	return match(Token::colon, 1);

      case ';':
	return match(Token::semicolon, 1);

      case ',':
	return match(Token::comma, 1);

      case '-':
	if (peek(1) == '>')
	  return match(Token::arrow, 2);
	return match(Token::minus, 1);

      case '+':
	return match(Token::plus, 1);

      case '*':
	return match(Token::star, 1);

      case '/':
	return match(Token::slash, 1);

      case '%':
	return match(Token::percent, 1);

      case '?':
	return match(Token::question, 1);

      case '=':
	if (peek(1) == '=')
	  return match(Token::equal_equal, 2);
	return match(Token::equal, 1);

      case '!':
	if (peek(1) == '=')
	  return match(Token::bang_equal, 2);

      case '<':
	if (peek(1) == '=')
	  return match(Token::less_equal, 2);
	return match(Token::less, 1);

      case '>':
	if (peek(1) == '=')
	  return match(Token::greater_equal, 2);
	return match(Token::greater, 1);
      }
    }

    return Token();
  }
  
  bool is_end_of_file() { return first == limit; }

private:
  char peek() const { return *first; }
  char peek(int i) const { return *(first + i); }

  char consume() { return *first++; }

  Token match(Token::Name n, int len) {
    std::string str(first, first+len);
    
    // Get symbol from table
    Symbol sym = sym_table->get(str);
    
    // Construct token 
    Token token = Token(n, sym);

    // Move lexer
    first += len;
    
    return token;
  }
  
  SymbolTable* sym_table;

  char const* first;
  char const* limit;

  int src_line;

  std::unordered_map<std::string, Token::Name> kws;
};
