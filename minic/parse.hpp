#pragma once

#include <cassert>

#include "statement.hpp"
#include "lexer.hpp"

class Parser {
public:
  Parser(SymbolTable& sym, std::string const& input)
    : lexer(sym, input)
  {
    // Add our tokens
    while (!lexer.is_end_of_file()) {
      Token t = lexer.get_next_token();
      tokens.push_back(t);
    }

    // Set the front and end
    current_tok = &tokens.front();
    limit = &tokens.back();
  }

  void parse_program();

  Expression* parse_expression() {
    Expression* expr = parse_assignment_expression();
    return expr;
  }
  
  Declaration* parse_declaration() {
    if (match(Token::fun_kw).get_name() == Token::fun_kw)
      return parse_function_definition();
    else if (match(Token::var_kw).get_name() == Token::var_kw)
      return parse_variable_definition();
  }

  Statement* parse_statement() {
    switch(current_tok->get_name()) {
    case Token::semicolon:
      return parse_empty_statement();
    case Token::if_kw:
      return parse_if_statement();
    case Token::while_kw:
      return parse_while_statement();
    case Token::break_kw:
      return parse_break_statement();
    case Token::continue_kw:
      return parse_continue_statement();
    case Token::return_kw:
      return parse_return_statement();
    case Token::lbrace:
      return parse_block_statement();
    }
  }

  Type* parse_type();
  
private:
  Token consume() {
    // Make sure we are not at the end of the file
    assert(current_tok != limit);
    
    Token t = *current_tok;
    ++current_tok;
    return t;
  }
  
  Token match(Token::Name n) {
    if (current_tok->get_name() == n)
      return consume();
    return Token();
  }

  // Parsing Expressions
  
  // FIXME
  // How to get the literals of the expression and the type
  Expression* parse_unary_expression() {
    if (match(Token::minus).get_name() == Token::minus)
      return nullptr;
    else if (match(Token::slash).get_name() == Token::slash)
      return nullptr;
    else if (match(Token::bang).get_name() == Token::bang)
      return nullptr;
  }

  Expression* parse_multiplicative_expression() {
    Expression* expr = parse_unary_expression();

    if (match(Token::star).get_name() == Token::star)
      return parse_multiplicative_expression();
    else if (match(Token::slash).get_name() == Token::slash)
      return parse_multiplicative_expression();
    else if (match(Token::percent).get_name() == Token::percent)
      return parse_multiplicative_expression();

    return parse_unary_expression();
  }

  Expression* parse_additive_expression() {
    Expression* expr = parse_multiplicative_expression();

    if (match(Token::plus).get_name() == Token::plus)
      return parse_additive_expression();
    if (match(Token::minus).get_name() == Token::minus)
      return parse_additive_expression();

    return expr;
  }

  Expression* parse_relational_expression() {
    Expression* expr = parse_additive_expression();

    if (match(Token::less).get_name() == Token::less)
      return parse_relational_expression();
    else if (match(Token::greater).get_name() == Token::greater)
      return parse_relational_expression();
    else if (match(Token::less_equal).get_name() == Token::less_equal)
      return parse_relational_expression();
    else if (match(Token::greater_equal).get_name() == Token::greater_equal)
      return parse_relational_expression();

    return expr;
  }

  Expression* parse_equality_expression() {
    Expression* expr = parse_relational_expression();

    if (match(Token::equal_equal).get_name() == Token::equal_equal)
      return parse_equality_expression();
    else if (match(Token::bang_equal).get_name() == Token::bang_equal)
      return parse_equality_expression();

    return expr;
  }

  Expression* parse_and_expression() {
    Expression* expr = parse_equality_expression();

    if (match(Token::and_kw).get_name() == Token::and_kw)
      return parse_and_expression();

    return expr;
  }

  Expression* parse_or_expression() {
    Expression* expr = parse_and_expression();

    if (match(Token::or_kw).get_name() == Token::or_kw)
      return parse_or_expression();
    
    return expr;
  }

  Expression* parse_conditional_expression() {
    Expression* expr = parse_or_expression();

    if (match(Token::question).get_name() == Token::question)
      return parse_expression();
  }
  
  Expression* parse_assignment_expression() {
    Expression* expr = parse_conditional_expression();

    if (match(Token::equal).get_name() == Token::equal)
      return parse_assignment_expression();

    return expr;
  }

  
  // Parsing statements
  Statement* parse_if_statement() {
    Expression* conditional = nullptr;
     if (match(Token::if_kw).get_name() == Token::if_kw)
       conditional = parse_expression();

    Statement* s1 = parse_statement();
    Statement* s2 = nullptr;

    if (match(Token::else_kw).get_name() == Token::else_kw)
      s2 = parse_statement();

    if (conditional->evaluate())
      return s1;
    else if (s2 != nullptr)
      return s2;
  }
  
  Statement* parse_block_statement() {
    if (match(Token::lbrace).get_name() == Token::lbrace)
      return parse_statement_sequence();
  }
    
  Statement* parse_return_statement() {
    return new Return_Stmt(parse_expression());
  }

  Statement* parse_while_statement() {
    Expression* expr = parse_expression();
    Statement* stmt = parse_statement();

    return stmt;
  }
    
  Statement* parse_continue_statement() {
    return new Continue_Stmt;
  }
  
  Statement* parse_empty_statement() { return nullptr; }
  
  Statement* parse_break_statement() { return new Break_Stmt; }

  Statement* parse_statement_sequence() {
    Statement* stmt = parse_statement();

    if (match(Token::rbrace).get_name() == Token::rbrace)
      return stmt;

    return parse_statement_sequence();
  }

  
  // Parsing declarations
  Declaration* parse_function_definition() {
    if (match(Token::lparen).get_name() == Token::lparen)
      return parse_parameter_list();
    // Return a function decl
  }
  Declaration* parse_variable_definition();
  Declaration* parse_object_defintion();
  Declaration* parse_reference_defintion();

  Declaration* parse_parameter_list() {
    Declaration* decl = parse_parameter();

    if (match(Token::comma).get_name() == Token::comma)
      return parse_parameter_list();

    return decl;
  }
  
  Declaration* parse_parameter() {
    // Expect an id
    assert(match(Token::identifier).get_name() == Token::identifier);
  }
  
  
  Lexer lexer;
  Token* current_tok;
  Token* limit;
  std::vector<Token> tokens;
  
};
