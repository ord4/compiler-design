#include <vector>

class Type {
public:
  virtual bool same_type(Type* that) = 0;
};

class Boolean_Type : public Type {
public:
  bool same_type(Type* that) override {
    return (dynamic_cast<Boolean_Type const*>(that) != nullptr) ? true : false;
  }
};

class Integer_Type : public Type {
public:
  bool same_type(Type* that) override {
    return (dynamic_cast<Integer_Type const*>(that) != nullptr) ? true : false;
  }
};

class Float_Type : public Type {
public:
  bool same_type(Type* that) override {
    return (dynamic_cast<Float_Type const*>(that) != nullptr) ? true : false;
  }
};

class Ref_Type : public Type {
public:
  bool same_type(Type* that) override {
    return (dynamic_cast<Ref_Type const*>(that) != nullptr) ? true : false;
  }
};

// Store the parameter types of the function in a vector, where the
// last element indicates the function return type
class Function_Type : public Type {
public:
  Function_Type(std::vector<Type*>& v) {
    params = v;
  }
  
  bool same_type(Type* that) override {
    return (dynamic_cast<Function_Type const*>(that) != nullptr) ? true : false;
  }

private:
  std::vector<Type*> params;
};

