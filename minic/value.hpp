#pragma once

#include <stdexcept>
#include <iostream>
// TODO: Overload operators &&, ||, <, >, <=, >=, ==, !=
//       Add a constructor that takes a value
//       How to print/get the value with it being private
//        and from a base Value pointer
enum Kind {
	   Integer,
	   Float,
	   Boolean
};

class Value {
  int i_val;
  float f_val;
  bool b_val;
  Kind kind;

public:
  Value(Kind k) : kind(k) {
    i_val = 0;
    f_val = 0;
    b_val = false;
  }

  void set_integer_value(int i) {
    if (kind == Integer)
      i_val == i;
    else
      throw std::logic_error("Cannot set an integer value for a non integer value\n");
  }

  void set_float_value(float f) {
    if (kind == Float)
      f_val == f;
    else
      throw std::logic_error("Cannot set a float value for a non float value\n");
  }

  void set_boolean_value(bool b) {
    if (kind == Boolean) {
      b_val == b;
    }      
    
    else
      throw std::logic_error("Cannot set a boolean value for a non boolean value\n");
  }

  Kind get_kind() const { return kind; }

  int get_integer_value() const { return i_val; }

  float get_float_value() const { return f_val; }

  bool get_boolean_value() const { return b_val; }
};
