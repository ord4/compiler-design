#pragma once

#include "type.hpp"
#include "value.hpp"

#include <iostream>

class Expression {
public:
  // FIXME: how to print the value of the expression with it being of
  //        different types and a private member?
  virtual void print() = 0;
  virtual Type* get_type() = 0;
  virtual Value* get_value() = 0;
  virtual bool is_well_typed() = 0;
  virtual Value* evaluate() = 0;
};

class Boolean_Literal : public Expression {
public:
  Boolean_Literal(bool b) {
    val = new Value(Boolean);
    val->set_boolean_value(b);
    type = new Boolean_Type();
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return get_type()->same_type(new Boolean_Type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) 
      return val;
    throw std::logic_error("Expression is not well typed\n");
  }

  Value* get_value() override {
    return val;
  }

  void print() override {
    std::cout << get_value()->get_boolean_value();
  }
  
private:
  Value* val;
  Type* type;
};

class Integer_Literal : public Expression {
public:
  Integer_Literal(int n) {
    val = new Value(Integer);
    val->set_integer_value(n);
    type = new Integer_Type();
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return get_type()->same_type(new Integer_Type());
  }

  Value* evaluate() override {
    if (this->is_well_typed())
      return val;
    throw std::logic_error("Expression is not well typed\n");
  }

  Value* get_value() override {
    return val;
  }

  void print() override {
    std::cout << get_value()->get_integer_value();
  }

private:
  Value* val;
  Type* type;
};

//class Id_Expression : public Expression {
  //public:
  //  Id_Expression(Type* t, Declaration* d) {
  //    expr_type = t;
    //    decl = d;
    //  }
  //
   //private:
  //  Type* expr_type;
  //  Declaration* decl;
  //};
//

class Land : public Expression {
public:
  Land(Type* t, Expression* expr1, Expression* expr2) {
    type = t;
    e1 = expr1;
    e2 = expr2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " && ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() && e2->evaluate()->get_boolean_value());
    }
    else 
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Lor : public Expression {
public:
  Lor(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " || ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() || e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

// TODO: How to evaluate a conditional?
class Conditional : public Expression {
  Conditional(Type* t, Expression* op1, Expression* op2, Expression* op3) {
    type = t;
    e1 = op1;
    e2 = op2;
    e3 = op3;
  }

  void print() override {
    std::cout << "(";
    std::cout << "If ";
    e1->print();
    std::cout << " then, ";
    e2->print();
    std::cout << ", else ";
    e3->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  // Expression* evaluate() override {
  //   // How?
  //   if (this->is_well_typed())
  //     // eval
  //   throw std::logic_error("Expression is not well typed\n");
  // }

  Expression* get_conditional_operand() const {
    return e1;
  }

  Expression* get_true_expression() const {
    return e2;
  }

  Expression* get_false_expression() const {
    return e3;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Expression* e3;
};

class Eq : public Expression {
  Eq(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " == ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  // FIXME: Add functionality for other types than Boolean
  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() == e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Neq : public Expression {
  Neq(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " != ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() != e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Lt : public Expression {
  Lt(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " < ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() < e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }
				 
  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Gt : public Expression {
  Gt(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " > ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() > e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Lte : public Expression {
  Lte(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " <= ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() <= e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Gte : public Expression {
  Gte(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " >= ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  Value* evaluate() override {
    if (this->is_well_typed()) {
      val = new Value(Boolean);
      val->set_boolean_value(e1->evaluate()->get_boolean_value() >= e2->evaluate()->get_boolean_value());
    }
    else
      throw std::logic_error("Expression is not well typed\n");

    return val;
  }

  Value* get_value() override { return val; }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
  Value* val;
};

class Add_Expression : public Expression {
public:
  Add_Expression(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " + ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  // FIXME: Do I need to overload these operators?
  // Expression* evaluate() override {
  //   return new Integer_Literal(e1->evaluate() + e2->evaluate());
  // } 

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
};

class Sub_Expression : public Expression {
public:
  Sub_Expression(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " - ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  // Expression* evaluate() override {
  //   return new Integer_Literal(e1->evaluate() - e2->evaluate());
  // } 

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
};

class Mult_Expression : public Expression {
  Mult_Expression(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " * ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  // Expression* evaluate() override {
  //   return new Integer_Literal(e1->evaluate() * e2->evaluate());
  // } 

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
};

class Quot_Expression : public Expression {
  Quot_Expression(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " / ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  // Expression* evaluate() override {
  //   return new Integer_Literal(e1->evaluate() + e2->evaluate());
  // } 

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
};

class Rem_Expression : public Expression {
  Rem_Expression(Type* t, Expression* op1, Expression* op2) {
    type = t;
    e1 = op1;
    e2 = op2;
  }

  void print() override {
    std::cout << "(";
    e1->print();
    std::cout << " % ";
    e2->print();
    std::cout << ")";
  }

  Type* get_type() override {
    return type;
  }

  bool is_well_typed() override {
    return e1->get_type()->same_type(e2->get_type());
  }

  // Expression* evaluate() override {
  //   return new Integer_Literal(e1->evaluate() % e2->evaluate());
  // } 

  Expression* get_lhs_operand() const {
    return e1;
  }

  Expression* get_rhs_opreand() const {
    return e2;
  }

private:
  Type* type;
  Expression* e1;
  Expression* e2;
};

class Negation : public Expression {
  Negation(Type* t, Expression* op1) {
    type = t;
    e1 = op1;
  }

  void print() override {
    std::cout << "-(";
    e1->print();
    std::cout << ")\n";
  }

  Type* get_type() override {
    return type;
  }

  // FIXME: How to get the value and negate it?
  // Expression* evaluate() override {
  //   return new Integer_Literal(-1*e1->evaluate()));
  // }

  Expression* get_operand() const {
    return e1;
  }

private:
  Type* type;
  Expression* e1;
};

class Reciprocal : public Expression {
  Reciprocal(Type* t, Expression* op1) {
    type = t;
    e1 = op1;
  }

  void print() override {
    std::cout << "1 / ";
    e1->print();
    std::cout << '\n';
  }

  Type* get_type() override {
    return type;
  }

  // Expression* evaluate() override {
  //   return new Float_Literal(1/e1->get_value());
  // }
   
  Expression* get_operand() const {
    return e1;
  }

private:
  Type* type;
  Expression* e1;
};
